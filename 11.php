<?php
// Inicializamos las variables
$numero = 0;
$numeros = [];
$media = 0;
$suma = 0;

// $numero es el numero de notas que tenemos que introducir
$numero = $_GET["numero"];


// Comprobamos que se ha pulsado calcular

//Si no se ha pulsado => carga el formulario
if (!isset($_GET["calcular"])) {
?>
    <form action="">
        <!-- 
            Hay que guardar la variable $numero en un input
            Si no lo guardas en un input se el valor se pierde y no lo podemos usar
            Para guardarlo usamos un input tipo hidden (oculto)
        -->
        <input type="hidden" name="numero" value="<?= $numero ?>">
        <?php
        // Iteramos desde 1 hasta el numero de notas a introducir + 1
        // Creamos tantos labels e inputs como el número de notas a introducir
        for ($i = 1; $i < $numero + 1; $i++) {

        ?>
            <div>
                <label for="nota<?= $i ?>">Nota <?= $i ?> </label>
                <input type="nota" name="nota<?= $i ?>" id="nota<?= $i ?>">
            </div>
        <?php
        }
        ?>
        <div>
            <button name="calcular">Calcular media</button>
        </div>
    </form>
<?php
} else {
    // Si se ha pulsado el boton calcular te vuelve a cargar el formulario
?>
    <form action="">

        <?php

        for ($i = 1; $i < $numero + 1; $i++) {

        ?>
            <div>
                <label for="nota<?= $i ?>">Nota <?= $i ?> </label>
                <input type="number" name="nota<?= $i ?>" id="nota<?= $i ?>">
            </div>
        <?php
        }
        ?>
        <div>
            <button name="calcular">Calcular media</button>
        </div>
    </form>
<?php
    //Además iteramos para 
    for ($i = 1; $i < $numero + 1; $i++) {
        //guardamos en el array numeros cada nota introducida
        $numeros[$i] = $_GET["nota$i"];
        // Cálculamos la suma de las notas
        $suma = $suma + $numeros[$i];
    }
    // Calculamos la media de las notas introducidas
    $media = $suma / $numero;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <div>
        Notas introducidas:
        <br>
        <?php
        foreach ($numeros as $valor) {
            echo ($valor . "<br>");
        }
        ?>
    </div>
    <div>Media de las notas introducidas: <?= $media ?></div>

</body>

</html>