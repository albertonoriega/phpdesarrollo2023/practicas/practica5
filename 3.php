<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php

    $dia="";

    //Comprobar si se ha cargado el formulario

    // Si no se ha pulsado el boton de enviar
    if (!isset($_GET["enviar"])) {
                
    ?>
    
    <form action="">
        <div>
            <label for="dia">Día de la semana</label>
            <input type="number" name="dia" id= "dia">
        </div>
        <div>
            <button name="enviar">Enviar</button>
        </div>
    </form>

    <?php

    } else {
    // Cuando se ha pulsado el botón => mostramos resultados
        $dia = $_GET["dia"];
        $diaDeLaSemana= "";

        switch ($dia) {
            case 1:
                $diaDeLaSemana = 'Lunes';
                break;
            case 2:
                $diaDeLaSemana = 'Martes';
                break;
            case 3:
                $diaDeLaSemana = 'Miercoles';
                break;
            case 4:
                $diaDeLaSemana = 'Jueves';
                break;
            case 5:
                $diaDeLaSemana = 'Viernes';
                break;
            case 6:
                $diaDeLaSemana = 'Sabado';
                break;
            case 7:
                $diaDeLaSemana = 'Domingo';
                break;
            default:
                $diaDeLaSemana = "Valor introducido no correcto";
                break;
        }

    
        ?>
    
        <div>Dia introducido: <?= $dia?></div>
        <div>Dia de la semana: <?= $diaDeLaSemana?></div>
    
        <?php
    }
    ?>

</body>
</html>