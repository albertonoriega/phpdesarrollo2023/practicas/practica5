<?php

$numero = "";
// Comprobamos que se ha pulsado enviar
if (isset($_GET["enviar"])) {
    // $numero toma el valor que se ha introducido en el formulario
    $numero = $_GET["numero"];
    do {
        //imprimimos cada numero en un div
        echo "<div>$numero</div>";
        // Cada iteración, a $numero le restamos 2
        $numero = $numero - 2;
    } while ($numero > 10); // repite iteración si $numero es mator que 10
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <form action="">
        <div>
            <label for="numero">Numero</label>
            <input type="number" name="numero" id="numero">
        </div>
        <div>
            <button name="enviar">Enviar</button>
        </div>
    </form>


</body>

</html>