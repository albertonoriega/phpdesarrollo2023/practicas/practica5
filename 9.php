<?php

$numero = "";
$suma = 0;

// Comprobamos que se ha pulsado enviar
if (isset($_GET["enviar"])) {
    // $numero toma el valor que se ha introducido en el formulario
    $numero = $_GET["numero"];
    do {
        // Comprobamos que el numero sea impar
        if ($numero % 2 == 1) {
            // si es impar, sumamos el numero a la variable suma
            $suma = $suma + $numero;
        }

        // Cada iteración, a $numero le restamos 1
        $numero--;
    } while ($numero > 0); // repite iteración si $numero es mayor que 0
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <form action="">
        <div>
            <label for="numero">Numero</label>
            <input type="number" name="numero" id="numero">
        </div>
        <div>
            <button name="enviar">Enviar</button>
        </div>
    </form>

    <div>Suma: <?= $suma ?></div>

</body>

</html>