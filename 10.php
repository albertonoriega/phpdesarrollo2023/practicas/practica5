<?php

$numero1 = "";
$numero2 = "";
$numero3 = "";
$numero4 = "";
$media = 0;

// Comprobamos que se ha pulsado enviar
if (isset($_GET["enviar"])) {
    // $numero toma el valor que se ha introducido en el formulario
    $numero1 = $_GET["numero1"];
    $numero2 = $_GET["numero2"];
    $numero3 = $_GET["numero3"];
    $numero4 = $_GET["numero4"];

    // Calculamos la media de las 4 notas

    $media = ($numero1 + $numero2 + $numero3 + $numero4) / 4;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <form action="">
        <div>
            <label for="numero1">Nota 1</label>
            <input type="number" name="numero1" id="numero1">
        </div>
        <div>
            <label for="numero2">Nota 2</label>
            <input type="number" name="numero2" id="numero2">
        </div>
        <div>
            <label for="numero3">Nota 3</label>
            <input type="number" name="numero3" id="numero3">
        </div>
        <div>
            <label for="numero4">Nota 4</label>
            <input type="number" name="numero4" id="numero4">
        </div>
        <div>
            <button name="enviar">Enviar</button>
        </div>
    </form>

    <div>Media de las 4 notas introducidas: <?= $media ?></div>

</body>

</html>